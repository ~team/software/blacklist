metadata.mymovies.dk::::[uses-nonprivacy] requires subscription to My Movies
plugin.audio.dr.dk.netradio::::[nonfree] contains a statement saying "Some parts of this addon may not be legal in your country of residence - please check with your local laws before installing." It is a contradiction when it comes to the idea of freedom under the GPL2.
plugin.audio.groove::::[uses-nonprivacy] requires subscription to Grooveshark
plugin.audio.pureradio:plugin.audio.pureradio:::[uses-nonprivacy] recommends join on Facebook
plugin.audio.qobuz::::[uses-nonprivacy] requires subscription to Qobuz music streaming service
plugin.audio.resetradio::::[nonfree] contains a statement saying "Some parts of this addon may not be legal in your country of residence - please check with your local laws before installing." It is a contradiction when it comes to the idea of freedom under the GPL3.
plugin.audio.wimp::::[uses-nonprivacy] requires subscription to TIDAL
plugin.dbmc::::[uses-nonprivacy] requires subscription to Dropbox
plugin.onedrive::::[uses-nonprivacy] requires subscription to OneDrive to play all media from OneDrive including videos, music and pictures
plugin.program.jdownloader::::[uses-nonfree] adapted to support nonfree JDownloader
plugin.program.utorrent::::[uses-nonfree] adapted to support nonfree uTorrent
plugin.programm.xbmcmail:plugin.programm.xbmcmail:::[uses-nonprivacy] refers GMail, Yahoo, AOL, iCloud as IMAP-based Email Providers examples to be used
plugin.video.arretsurimages::::[uses-nonprivacy] requires subscription to the French site arretsurimages.net
plugin.video.cbc::::[nonfree] contains a description saying "only for use in Canada" It is a contradiction when it comes to the idea of freedom under the GPL2.
plugin.video.cbcnews::::[nonfree] contains a description saying "only for use in Canada" It is a contradiction when it comes to the idea of freedom under the GPL2.
plugin.video.crunchyroll-takeout::::[uses-nonprivacy] requires subscription to Crunchyroll
plugin.video.dailymotion_com::::[nonfree] contains a statement saying "Some parts of this addon may not be legal in your country of residence - please check with your local laws before installing." It is a contradiction when it comes to the idea of freedom under the GPL2.
plugin.video.dplay::::[nonfree] contains a statement saying "Some parts of this addon may not be legal in your country of residence. Please check with your local laws before installing." It is a contradiction when it comes to the idea of freedom under the GPL3.
plugin.video.dr.dk.bonanza::::[nonfree] contains a statement saying "Some parts of this addon may not be legal in your country of residence - please check with your local laws." It is a contradiction when it comes to the idea of freedom under the GPL2.
plugin.video.et.canada::::[nonfree] contains a description saying "only for use in Canada" It is a contradiction when it comes to the idea of freedom under the GPL2.
plugin.video.fattoquotidianotv::::[nonfree] contains a statement saying "Some parts of this addon may not be legal in your country of residence - please check with your local laws before installing." It is a contradiction when it comes to the idea of freedom under the GPL3.
plugin.video.fsgo::::[uses-nonprivacy] requires subscription to one of FOX Sports GO's participating TV providers
plugin.video.funimationnow::::[nonfree] contains a statement saying "Some parts of this addon may not be legal in your country of residence - please check with your local laws before installing." It is a contradiction when it comes to the idea of freedom under the GPL3.
plugin.video.iplayerwww::::[nonfree] a UK TV Licence is required to watch the live, catch-up, or on-demand TV content -> https://www.gov.uk/tv-licence
plugin.video.irishtv::::[uses-nonprivacy] requires subscription to AerTV
plugin.video.lacosa::::[nonfree] contains a statement saying "Some parts of this addon may not be legal in your country of residence - please check with your local laws before installing." It is a contradiction when it comes to the idea of freedom under the GPL3.
plugin.video.longnow::::[uses-nonprivacy] requires subscription to Long Now Foundation
plugin.video.mediacorp:plugin.video.mediacorp:::[uses-nonfree] recommends nonfree Arial font
plugin.video.mtv_de::::[nonfree] contains a statement saying "Some parts of this addon may not be legal in your country of residence - please check with your local laws before installing."
plugin.video.mtv.it::::[nonfree] contains a statement saying "Some parts of this addon may not be legal in your country of residence - please check with your local laws before installing." It is a contradiction when it comes to the idea of freedom under the GPL3.
plugin.video.myvideo_de::::[nonfree] contains a statement saying "Some parts of this addon may not be legal in your country of residence - please check with your local laws before installing." It is a contradiction when it comes to the idea of freedom under the GPL2.
plugin.video.mytv_bg::::[uses-nonprivacy] requires subscription to MyTV.bg
plugin.video.nba::::[uses-nonprivacy] requires subscription to the NBA International League Pass
plugin.video.nfl.gamepass::::[uses-nonprivacy] requires subscription to NFL Game Pass
plugin.video.nhl-gamecenter-live::::[uses-nonprivacy] requires subscription to NHL GameCenter
plugin.video.nick_de::::[nonfree] contains a statement saying "Some parts of this addon may not be legal in your country of residence - please check with your local laws before installing." It is a contradiction when it comes to the idea of freedom under the GPL2.
plugin.video.nolife::::[uses-nonprivacy] requires subscription to Nolife Online
plugin.video.pcloud-video-streaming::::[uses-nonprivacy] requires subscription to pCloud
plugin.video.previewnetworks::::[nonfree] contains a statement saying "Some parts of this addon may not be legal in your country of residence - please check with your local laws before installing." It is a contradiction when it comes to the idea of freedom under the GPL2.
plugin.video.psvue::::[uses-nonprivacy][nonfree] requires subscription to PS Vue which is currently only available in the US
plugin.video.putio::::[uses-nonprivacy] requires subscription to Put.io
plugin.video.radbox:plugin.video.radbox:::[uses-nonprivacy] recommends subscription to Twitter and Facebook video streams
plugin.video.s04tv::::[uses-nonprivacy] requires subscription to S04.tv
plugin.video.schaetzederwelt::::[nonfree] contains a statement saying "Some parts of this addon may not be legal in your country of residence - please check with your local laws before installing." It is a contradiction when it comes to the idea of freedom under the GPL3.
plugin.video.serviziopubblico::::[nonfree] contains a statement saying "Some parts of this addon may not be legal in your country of residence - please check with your local laws before installing." It is a contradiction when it comes to the idea of freedom under the GPL3.
plugin.video.servustv_com::::[nonfree] contains a statement saying "Some parts of this addon may not be legal in your country of residence - please check with your local laws before installing." It is a contradiction when it comes to the idea of freedom under the GPL2.
plugin.video.showcase.canada::::[nonfree] contains a description saying "only for use in Canada" It is a contradiction when it comes to the idea of freedom under the GPL2.
plugin.video.slice.canada::::[nonfree] contains a description saying "only for use in Canada" It is a contradiction when it comes to the idea of freedom under the GPL2.
plugin.video.simpsonsworld::::[uses-nonprivacy] only accessible to authenticated FX subscribers
plugin.video.skytg24::::[nonfree] contains a statement saying "Some parts of this addon may not be legal in your country of residence - please check with your local laws before installing." It is a contradiction when it comes to the idea of freedom under the GPL3.
plugin.video.southpark_unofficial::::[nonfree] contains a statement saying "Some parts of this addon may not be legal in your country of residence - please check with your local laws before installing." It is a contradiction when it comes to the idea of freedom under the GPL2.
plugin.video.spiegel_tv::::[nonfree] contains a statement saying "Some parts of this addon may not be legal in your country of residence - please check with your local laws before installing." It is a contradiction when it comes to the idea of freedom under the GPL2.
plugin.video.svtplay::::[nonfree] contains a statement saying "Some parts of this addon may not be legal in your country of residence - please check with your local laws before installing." It is a contradiction when it comes to the idea of freedom under the GPL3.
plugin.video.tele5_de::::[nonfree] contains a statement saying "Some parts of this addon may not be legal in your country of residence - please check with your local laws before installing." It is a contradiction when it comes to the idea of freedom under the GPL2.
plugin.video.thenewboston::::[uses-nonfree] contains free educational video tutorials on nonfree Adobe software
plugin.video.time_com::::[nonfree] contains a statement saying "Some parts of this addon may not be legal in your country of residence - please check with your local laws before installing." It is a contradiction when it comes to the idea of freedom under the GPL2.
plugin.video.tivi_de::::[nonfree] contains a statement saying "Some parts of this addon may not be legal in your country of residence - please check with your local laws before installing." It is a contradiction when it comes to the idea of freedom under the GPL2.
plugin.video.tv3play.dk::::[nonfree] contains a statement saying "Some parts of this addon may not be legal in your country of residence - please check with your local laws." It is a contradiction when it comes to the idea of freedom under the GPL2.
plugin.video.tyt::::[uses-nonprivacy] requires subscription to TYT Network
plugin.video.viaplay::::[uses-nonprivacy] requires subscription to Viaplay
plugin.video.vimcasts:plugin.video.vimcasts:::[technical] uses the name “Linux” ambiguously
plugin.video.virginradio.it::::[nonfree] contains a statement saying "Some parts of this addon may not be legal in your country of residence - please check with your local laws before installing." It is a contradiction when it comes to the idea of freedom under the GPL3.
plugin.video.watson::::[uses-nonprivacy] requires subscription to Watson
plugin.video.yogaglo::::[uses-nonprivacy] requires subscription to YogaGlo
plugin.video.zattoobox::::[uses-nonprivacy] requires subscription to Zattoo to play Live TV and recorded programs
plugin.video.zdf_de_2016::::[nonfree] contains a statement saying "Some parts of this addon may not be legal in your country of residence - please check with your local laws before installing." It is a contradiction when it comes to the idea of freedom under the GPL2.
resource.images.languageflags-flat::::[nonfree] contains nonfree license
script.advanced.wol:script.advanced.wol:::[uses-nonfree] recommends use Windows to check of a successful wake-up via ping is not possible or fails on a system, [technical] uses the name “Linux” ambiguously
script.cinema.experience::::[nonfree] Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License
script.cinemavision::::[nonfree] Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International
script.facebook.media::::[uses-nonprivacy] requires subscription to Facebook
script.game.whatthemovie::::[uses-nonprivacy] requires subscription to whatthemovie.com
script.module.adobepass::::[uses-nonfree][uses-nonprivacy] useful only for SaaSS and nonprivacy services that requires Adobe Pass
script.module.protobuf::::[nonfree] contains nonfree license
script.module.pyamf:script.module.pyamf:::[uses-nonfree] recommends nonfree Adobe Flash and Adobe Integrated Runtime
script.module.pydevd::::[nonfree] contains EPL license, incompatible with GPL -> https://www.gnu.org/licenses/license-list.html#EPL
script.plex::::[uses-nonprivacy] requires subscription to Plex Pass
script.screensaver.nyancat::parabola:329:[uses-nonfree] uses Nyan Cat which is a nonfree artwork
script.trakt::::[uses-nonfree] recommends mobile apps for iPhone, iPad, Android, and Windows Phone, [uses-nonprivacy] requires subscription to Trakt.tv
script.video.funimationnow::::[nonfree] contains a statement saying "Some parts of this addon may not be legal in your country of residence - please check with your local laws before installing." It is a contradiction when it comes to the idea of freedom under the GPL3.
plugin.video.tvpvod::::[nonfree] missing license file
script.web.viewer:script.web.viewer:::[uses-nonprivacy] contains a description saying "(ie. facebook,flickr etc.)" as application authorization examples
service.fhemcinema::::[semifree] is under the GPL2, however the plugin logo is under copyright by bytefeed.com without license -> https://www.gnu.org/licenses/license-list.html#NoLicense
service.linuxwhatelse.notify:service.linuxwhatelse.notify:::[uses-nonprivacy] recommends join the linuxwhatelse community subscribing to Google+ to participate in the beta-test
service.scrobbler.lastfm::::[nonprivacy] contains a notification saying "The Last.fm scrobbler will submit info of the songs you've been listening to in Kodi to last.fm"
service.subtitles.argenteam::::[nonfree] contains a Spanish description saying "Los subtítulos, por lo tanto, no son realizados con fines comerciales y queda expresamente prohibido su uso con esta finalidad." which translated into English is "Subtitles, therefore, are not made for commercial purposes and it is expressly prohibited its use for this purpose." It is nonfree and a contradiction when it comes to the idea of freedom under the GPL2.
service.subtitles.opensubtitles::::[uses-nonprivacy] requires subscription to OpenSubtitles.org
skin.aeon.nox.5::::[nonfree] CC BY-NC-SA 4.0
skin.arctic.zephyr::::[nonfree] Creative Commons Non Commercial 3.0 License
skin.bello.6::::[nonfree] Creative Commons Attribution-NonCommercial-Share Alike 4.0 International License
skin.eminence.2::::[nonfree] CC-NC-SA 3.0
skin.fuse.neue::::[nonfree] Creative Commons Non Commercial 3.0 License
skin.mimic::::[nonfree] CC BY-NC-SA 4.0
skin.omni::::[nonfree] CC BY-NC-SA 4.0
skin.pellucid::::[nonfree] Creative Commons Attribution-Noncommercial-Share Alike 3.0
skin.phenomenal::::[nonfree] Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License
skin.rapier::::[nonfree] Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported
skin.xperience1080::::[nonfree] Creative Commons Attribution-Noncommercial-Share Alike 3.0
weather.metoffice::::[nonfree] contains a statement saying "In using this addon you agree to the terms and conditions specified under the Met Office Datapoint terms and conditions. http://www.metoffice.gov.uk/datapoint/terms-conditions. Data provided by Met Office Datapoint is covered by the Open Government License. http://www.nationalarchives.gov.uk/doc/open-government-licence/version/2/. Use at your own risk. The authors accept no liability." which is nonfree. It is a contradiction when it comes to the idea of freedom under the GPL2.
weather.ozweather::::[uses-nonprivacy] leaks user location / API logs user data
weather.wunderground::::[nonfree] contains a statement saying "Use of this add-on implies that you have agreed to the Terms of Service located at http://www.wunderground.com/weather/api/d/terms.html" which is nonfree. It is a contradiction when it comes to the idea of freedom under the GPL2.
weather.yahoo::::[nonfree] contains a statement saying "Use of this add-on implies that you have agreed to the Terms of Service located at http://developer.yahoo.com/weather/#terms" which is nonfree. It is a contradiction when it comes to the idea of freedom under the GPL2.
webinterface.awxi:webinterface.awxi:::[uses-nonfree] recommends nonfree software
